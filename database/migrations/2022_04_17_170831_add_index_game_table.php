<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('games', function (Blueprint $table) {
            $table->index(['winner_line_id'], 'winner_line_index');
            $table->index(['symbol_id'], 'symbol_index');
            $table->index(['campaign_id'], 'campaign_index');
            $table->index(['prize_id'], 'prize_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('games', function (Blueprint $table) {
            $table->dropIndex('winner_line_index');
            $table->dropIndex('symbol_index');
            $table->dropIndex('campaign_index');
            $table->dropIndex('prize_index');
        });
    }
};
