<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('games', function (Blueprint $table) {
            $table->bigInteger('winner_line_id')->nullable()->after('prize_id');
            $table->bigInteger('symbol_id')->nullable()->after('winner_line_id');
            $table->double('points')->nullable()->after('symbol_id');
            $table->double('total_points')->nullable()->after('points');
            $table->longText('matrix')->nullable()->after('total_points');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('games', function (Blueprint $table) {
            $table->dropColumn('winner_line_id');
            $table->dropColumn('symbol_id');
            $table->dropColumn('points');
            $table->dropColumn('total_points');
            $table->dropColumn('matrix');
        });
    }
};
