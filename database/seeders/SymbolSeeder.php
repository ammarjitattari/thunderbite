<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Symbol;
use Carbon\Carbon;

class SymbolSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Symbol::truncate();
        $currentTime = Carbon::now()->toDateTimeString();
        $symbols = [
            [
                'symbol' => 'apple.jpg',
                'status' => Symbol::STATUS_ACTIVE,
                'points' => 1,
                'created_at' => $currentTime,
                'updated_at' => $currentTime
            ],
            [
                'symbol' => 'Banana.jpg',
                'status' => Symbol::STATUS_ACTIVE,
                'points' => 2,
                'created_at' => $currentTime,
                'updated_at' => $currentTime
            ],
            [
                'symbol' => 'guava.jpg',
                'status' => Symbol::STATUS_ACTIVE,
                'points' => 3,
                'created_at' => $currentTime,
                'updated_at' => $currentTime
            ],
            [
                'symbol' => 'orange.jpg',
                'status' => Symbol::STATUS_ACTIVE,
                'points' => 4,
                'created_at' => $currentTime,
                'updated_at' => $currentTime
            ],
            [
                'symbol' => 'pineapple.jpg',
                'status' => Symbol::STATUS_ACTIVE,
                'points' => 5,
                'created_at' => $currentTime,
                'updated_at' => $currentTime
            ],
            [
                'symbol' => 'plum.jpg',
                'status' => Symbol::STATUS_ACTIVE,
                'points' => 6,
                'created_at' => $currentTime,
                'updated_at' => $currentTime
            ]
        ];
        Symbol::insert($symbols);
    }
}
