<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\WinnerLine;
use Carbon\Carbon;

class WinnerLineSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        WinnerLine::truncate();
        $currentTime = Carbon::now()->toDateTimeString();
        $winnerLines = [
            [
                'line' => json_encode([1,2,3,4,5]),
                'status' => WinnerLine::STATUS_ACTIVE,
                'created_at' => $currentTime,
                'updated_at' => $currentTime
            ],
            [
                'line' => json_encode([6,7,8,9,10]),
                'status' => WinnerLine::STATUS_ACTIVE,
                'created_at' => $currentTime,
                'updated_at' => $currentTime
            ],
            [
                'line' => json_encode([11,12,13,14,15]),
                'status' => WinnerLine::STATUS_ACTIVE,
                'created_at' => $currentTime,
                'updated_at' => $currentTime
            ],
            [
                'line' => json_encode([1,7,13,9,5]),
                'status' => WinnerLine::STATUS_ACTIVE,
                'created_at' => $currentTime,
                'updated_at' => $currentTime
            ],
            [
                'line' => json_encode([11,7,3,9,15]),
                'status' => WinnerLine::STATUS_ACTIVE,
                'created_at' => $currentTime,
                'updated_at' => $currentTime
            ],
            [
                'line' => json_encode([6,2,3,4,10]),
                'status' => WinnerLine::STATUS_ACTIVE,
                'created_at' => $currentTime,
                'updated_at' => $currentTime
            ],
            [
                'line' => json_encode([6,12,13,14,10]),
                'status' => WinnerLine::STATUS_ACTIVE,
                'created_at' => $currentTime,
                'updated_at' => $currentTime
            ],
            [
                'line' => json_encode([1,2,8,14,15]),
                'status' => WinnerLine::STATUS_ACTIVE,
                'created_at' => $currentTime,
                'updated_at' => $currentTime
            ],
            [
                'line' => json_encode([11,12,8,4,5]),
                'status' => WinnerLine::STATUS_ACTIVE,
                'created_at' => $currentTime,
                'updated_at' => $currentTime
            ]
        ];
        WinnerLine::insert($winnerLines);
    }
}
