<?php
namespace App\Services;

use App\Models\WinnerLine;
use App\Models\Symbol;
use Carbon\Carbon;

class GameService {

    /**
     * It spin and return spinner data
     * 
     * @param integer $campaignId
     * @param string $user
     * @return array
     */
    public function runSpinner($campaignId, $user)
    {
        $gameMatrix = $this->createGameMatrix();
        $spinnerData = $this->spin($gameMatrix);
        $data = [
            'campaign_id' => $campaignId,
            'account' => $user,
            'matrix' => $gameMatrix,
            'revealed_at' => Carbon::now()->toDateTimeString()
        ];

        // Saving spinner data
        if(!empty($spinnerData)){
            $data['symbol_id'] = $spinnerData['symbol_id'];
            $data['winner_line_id'] = $spinnerData['winner_line_id'];
            $symbol = Symbol::find( $data['symbol_id']);
            $matchingColumns = $spinnerData['matching_columns'];
            $totalPoints = $symbol->points*count($matchingColumns);
            $data['total_points'] = $totalPoints;
            $data['points'] = $symbol->points;
        }
       
        return [
            'matrix' => $gameMatrix,
            'game_data' => $data
        ];
    }
     /**
     * It creates a game matrix
     * 
     * @return array
     */
    public function createGameMatrix()
    {
        $symbols = Symbol::activeSymbols()->pluck('id')->toArray();
        $game = [];
        // Creating game randon matrix
        for($i=1;$i<=3;$i++){
            $row=[];
            for($j=1;$j<=5;$j++){
                // storing rand array symbols from active symbol
                $row[] = $symbols[array_rand($symbols,1)];
            }
            $game[] = $row;
        }

        return $game;
    }
    /**
     * It returns the winner line data if use win else return empty array.
     * 
     * @param  array $gameMatrix
     * @return array
     */

    public function spin($gameMatrix)
    {
        $winnerLines = WinnerLine::activeWinnerLines()->pluck('line','id');
        $winnerArray = [];

        // Winner line loop
        foreach($winnerLines as $winnerLineId=>$winnerLine){
            $winnerData = $this->hasMatchingLine($gameMatrix,$winnerLine);
            if(!empty($winnerData)){
                $winnerData['winner_line_id'] = $winnerLineId;
                return $winnerData;
            }
        }

        return $winnerArray;
    }

    /**
     * It checkes matrix has matching symbol of passed winner line
     * 
     * @param array $gameMatrix
     * @param array $winnerLine
     * @return array|null
     */
    public function hasMatchingLine($gameMatrix, $winnerLine)
    {
        $rowValues = [];
        $matchingSymbolId = null;
        $oldValue = null;
        $matchingColumns = [];

        // Checking winner line exist in game martix
        foreach($winnerLine as $value){
            //Logic for game martix index
            $index = $value - 1;
            $arrayOneIndex = 0;
            if($index > 4){
                $arrayOneIndex = floor($index/5);
            }
            $arrayTwoIndex = $index%5;

            // Getting value from game matrix 
            $currentValue = isset($gameMatrix[$arrayOneIndex][$arrayTwoIndex]) ? $gameMatrix[$arrayOneIndex][$arrayTwoIndex] : null;
            $rowValues[] = $currentValue;
            
            // checking last winner column value matching with current value
            if($oldValue !== null && !empty($currentValue) && $currentValue == $oldValue){
                $matchingSymbolId = $currentValue;
                $matchingColumns[] = $value;
            }else{
                $matchingColumns = [];
                $matchingSymbolId = $currentValue;
                $matchingColumns[] = $value;
            }
            $oldValue = $currentValue;
        }

        //Checking winner matching line has 3 or more than 3 values
        if(count($matchingColumns) >= 3){
            return [
                'symbol_id' => $matchingSymbolId,
                'matching_columns' =>$matchingColumns,
                'winner_line'=> $winnerLine
            ];
        }

        return null;
        
    }
}