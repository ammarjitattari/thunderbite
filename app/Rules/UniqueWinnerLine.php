<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Models\WinnerLine;

class UniqueWinnerLine implements Rule
{
    /**
     * Use for App\Models\WinnerLine object
     */
    public $winnerLine;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->winnerLine = new WinnerLine;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $id = request()->route('winner_line');
        return !$this->winnerLine->isExists($value, $id);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('validation.custom.winner_line.unique');
    }
}
