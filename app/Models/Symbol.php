<?php

namespace App\Models;

use DateTimeZone;
use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\Types\Boolean;

class Symbol extends Model
{
    const STATUS_ACTIVE = '1';
    const STATUS_INACTIVE = '0';
    const MAXIMUM_ACTIVE_SYMBOLS = 10;
    const MIMIMUM_ACTIVE_SYMBOLS = 6;
    const SYMBOL_FOLDER = 'img\symbols';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'symbol', 'points', 'status', 'created_at', 'updated_at'
    ];

    protected $dates = ['created_at', 'updated_at'];
    /**
     * Mutator for setting Symbol full URL
     */
    public function getSymbolUrlAttribute()
    {
        return $this->symbol ? asset(static::SYMBOL_FOLDER.'/'.$this->symbol):null;
    }

    /**
     * Mutator for setting Symbol full path
     */
    public function getSymbolPathAttribute()
    {
        return $this->symbol ? public_path(static::SYMBOL_FOLDER.'/'.$this->symbol):null;
    }
    /**
     * Use for search in admin panel
     */
    public function scopeSearch($query, $search)
    {
        if(!empty($search)){
            $query->where('symbols', 'like', '%'.$search.'%')
            ->orWhere('points', 'like', '%'.$search.'%');
        }
        return $query;
    }

    /**
     * It returns array of status
     * 
     * @return array
     */
    public static function getStatus()
    {
        return [
            static::STATUS_INACTIVE => 'Inactive',
            static::STATUS_ACTIVE => 'Active'
        ];
    }
    /**
     * It returns true if active symbols are with in range
     * 
     * @return boolean
     */
    public function canSpin():bool
    {
        $activeSymbols = static::where('status', static::STATUS_ACTIVE)->count();
        return $activeSymbols >= static::MIMIMUM_ACTIVE_SYMBOLS && $activeSymbols <= static::MAXIMUM_ACTIVE_SYMBOLS;
    }
    
    /**
     * Return active symbols maximum 10
     * 
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function ScopeActiveSymbols($query)
    {
        return $query->where('status', static::STATUS_ACTIVE)->take(static::MAXIMUM_ACTIVE_SYMBOLS)->get();
    }

    /**
     * It returns true if admin can change the status
     * @param string @status
     * @param integer @id
     * @return boolean
     */
    public function canChangeStatus($status, $id = null):bool
    {
        $query = static::where('status', static::STATUS_ACTIVE);
        
        if($id){
            $query->where('id', '!=', $id);
        }
        return (
            $status == static::STATUS_ACTIVE && $query->count() < static::MAXIMUM_ACTIVE_SYMBOLS
        )
        ||
        (
            $status == static::STATUS_INACTIVE && $query->count() > static::MIMIMUM_ACTIVE_SYMBOLS
        );
    }

}
