<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WinnerLine extends Model
{
    use HasFactory;

    protected $fillable = ['line', 'status'];
    const STATUS_ACTIVE = '1';
    const STATUS_INACTIVE = '0';

    /**
     * It returns active winner lines
     */
    public function scopeActiveWinnerLines()
    {
        return static::where('status',static::STATUS_ACTIVE);
    }

    /**
     * It will encode line string to array
     *
     * @return  array
     */
    public function setLineAttribute($value)
    {
        $line = explode('-',$value);
        $line = array_map(function($value){ return (int) $value;  }, $line);
        $this->attributes['line'] = json_encode($line);
    }
    /**
     * It will decode line json to array
     *
     * @return  array
     */
    public function getLineAttribute($value)
    {
        return json_decode($value);
    }

    /**
     * It convert array to string can access as line_string
     *
     * @return  array
     */
    public function getLineStringAttribute()
    {
        return $this->line ? implode('-', $this->line):'';
    }

    /**
     * It return array of status
     * 
     * @return array
     */

    public static function getStatus()
    {
        return [
            static::STATUS_INACTIVE => 'Inactive',
            static::STATUS_ACTIVE => 'Active'
        ];
    }
    /**
     * This function use for filter result for admin
     */
    public function scopeSearch($query, $search)
    {
        if(!empty($search)){
            $query->where('line', 'like', '%'.$search.'%')
            ->orWhere('points', 'like', '%'.$search.'%');
        }
        return $query;
    }
    /**
     * It check line is exists in table 
     * 
     * @param string $line
     * @param integer $id
     * @return bool
     */
    public function isExists(string $line, $id = null)
    {
        // Converting string to array
        $line = explode('-',$line);
        // Type casting every value into integer
        $line = array_map(function($value){ return (int) $value;  }, $line);
        $line = json_encode($line);
        $query = static::where('line',$line);
        if($id){
            $query->where('id', '!=', $id);
        }
        
        return (bool) $query->count();
    }

    
}
