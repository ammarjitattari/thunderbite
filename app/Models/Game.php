<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\WinnerLine;
use App\Services\GameService;


class Game extends Model
{
    use HasFactory;

    protected $fillable = ['campaign_id', 'prize_id', 'account', 'revealed_at', 'symbol_id', 'points', 'total_points', 'winner_line_id','matrix'];

    protected $dates = [
        'revealed_at',
    ];

    /**
     * Use for GameService
     */
    private $__gameService;

    public function __construct()
    {
        $this->__gameService = new GameService;
    }

    public function scopeFilter($query)
    {
        $campaign = Campaign::find(session('activeCampaign'));

        
        if ($data = request('filter1')) {
            $query->where('account', 'like', $data.'%');
        }

        if ($data = request('filter2')) {
            $query->where('prize_id', $data);
        }

        if ($data = request('filter3')) {
            $query->whereRaw('HOUR(revealed_at) >= '.$data);
        }

        if ($data = request('filter4')) {
            $query->whereRaw('HOUR(revealed_at) <= '.$data);
        }

        $query->leftJoin('prizes', 'prizes.id', '=', 'games.prize_id')
            ->leftJoin('winner_lines', 'winner_lines.id', '=', 'games.winner_line_id')
            ->leftJoin('symbols', 'symbols.id', '=', 'games.symbol_id')
            ->select('games.id', 'account', 'prize_id', 'revealed_at', 'prizes.title', 'winner_lines.line as winner_line', 'total_points', 'symbols.symbol as symbol_url')
            ->where('games.campaign_id', $campaign->id);

        return $query;
    }

    /**
     * Mutator for setting Symbol full URL
     */
    public function getSymbolUrlAttribute($value)
    {
        return $value ? asset(Symbol::SYMBOL_FOLDER.'/'.$value):null;
    }

    private static function filterDates($query, $campaign): void
    {
        if (($data = request('date_start')) || ($data = Carbon::now()->subDays(6))) {
            $data = Carbon::parse($data)->setTimezone($campaign->timezone)->toDateTimeString();
            $query->where('games.revealed_at', '>=', $data);
        }
        if (($data = request('date_end')) || ($data = Carbon::now())) {
            $data = Carbon::parse($data)->setTimezone($campaign->timezone)->toDateTimeString();
            $query->where('games.revealed_at', '<=', $data);
        }
    }

    public function campaign()
    {
        return $this->belongsTo(Campaign::class);
    }
    /**
     * It return winner line as string with separated by hyphen(-)
     * 
     * @return string
     */
    public function getWinnerLineAttribute($value)
    {
        if(!empty($value)){
            $value = json_decode($value, true);
            return implode('-', $value);
        }
        return $value;
    }

    public function setMatrixAttribute($value)
    {
        $this->attributes['matrix'] = is_array($value) ? json_encode($value):$value;
    }

    public function prize()
    {
        return $this->belongsTo(Prize::class);
    }

    /**
     * It spin and save spinner data into database
     * 
     * @param integer $campaignId
     * @param string $user
     * @return array
     */
    public function saveSpinner($campaignId, $user)
    {
        $spinner = $this->__gameService->runSpinner($campaignId, $user);
        $game = static::create($spinner['game_data']);
        
        return [
            'matrix' => $spinner['matrix'],
            'game' => $game
        ];
    }
    /**
     * Return total spin
     * 
     * @param string $date
     * @param string $account
     * @param integer $maxSpin
     * 
     * @return integer
     */
    public function totalSpin($date, $account)
    {
        return static::whereDate('revealed_at', $date)->where('account', $account)->count();
    }

   
}
