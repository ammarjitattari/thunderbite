<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Game;
use App\Models\Symbol;
use Carbon\Carbon;

class Spinner extends Component
{
    
    /**
     * Use for show spinner button
     */
    public $canShowSpinner = false;

    /**
     * Active Campaign object
     */
    public $campaign;

    /**
     * Game user
     */
    public $campaignUser;

    /**
     * Storing spinner data after spinner
     */
    public $spinnerData;

    /**
     * Use for storing boolean value to spinner ran or not
     */
    public $isPlayed=false;

    /**
     * Use for message after spinner 
     */
    public $message;

    /**
     * Game mode object
     */

    private $__game;
    /**
     * Symbol mode object
     */
    private $__symbol;

    /**
     * Total spinned by user
     */
    public $totalSpin;

    /**
     * maximum spin use can do
     */
    public $spin;

    

    public function __construct()
    {
        $this->__game  = new Game;
        $this->__symbol  = new Symbol;
    }

    public function render()
    {
        $date = Carbon::now()->toDateString();
        $this->totalSpin = $this->__game->totalSpin($date, $this->campaignUser);
        // Checking user runn the spinner
        if($this->totalSpin < $this->spin && $this->campaign->is_started){
            $this->canShowSpinner = $this->__symbol->canSpin();
        }
        

        return view('livewire.spinner');
    }
    
    /**
     * It spin the game and save data into database
     */
    public function saveSpinner()
    {
        // Running spinner and saving values in DB
        if($this->spinnerData = $this->__game->saveSpinner($this->campaign->id, $this->campaignUser)){
            $this->canShowSpinner = false;
            $this->isPlayed = true;
            if($this->spinnerData['game']->points){
                $this->message = trans('messages.spinner.message.winner',['total_points'=> $this->spinnerData['game']->total_points]);
            }else{
                $this->message = trans('messages.spinner.message.lost');
            }
            
        }
    }

    

}
