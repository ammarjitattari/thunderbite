<?php

namespace App\Http\Livewire\Backstage;

use App\Models\Symbol;

class SymbolTable extends TableComponent
{
    public $sortField = 'id';

    public function render()
    {
        $columns = [
            [
                'title' => 'symbol',
                'field_type'=>'image',
                'url' => 'symbol_url',
                'sort' => false,
            ],
            [
                'title' => 'points',
                'sort' => true,
            ],
            [
                'title' => 'status',
                'field_type'=>'status',
                'status' => Symbol::getStatus(),
                'sort' => true,
            ],
            [
                'title' => 'Created at',
                'attribute' => 'created_at',
                'sort' => true,
            ],
            [
                'title' => 'Updated at',
                'attribute' => 'updated_at',
                'sort' => true,
            ],
        ];

        $columns[] = [
            'title' => 'tools',
            'sort' => false,
            'tools' => ['edit', 'delete'],
        ];

        return view('livewire.backstage.table', [
            'columns' => $columns,
            'resource' => 'symbols',
            'rows' => Symbol::search($this->search)
                ->orderBy($this->sortField, $this->sortAsc ? 'DESC' : 'ASC')
                ->paginate($this->perPage),
        ]);
    }
}
