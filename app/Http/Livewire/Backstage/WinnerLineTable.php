<?php

namespace App\Http\Livewire\Backstage;

use App\Models\WinnerLine;

class WinnerLineTable extends TableComponent
{
    public $sortField = 'id';

    public function render()
    {
        $columns = [
            [
                'title' => 'Winner line',
                'attribute'=>'line_string',
                'sort' => false,
            ],
            [
                'title' => 'status',
                'field_type'=>'status',
                'status' => WinnerLine::getStatus(),
                'sort' => true,
            ],
            [
                'title' => 'Created at',
                'attribute'=>'created_at',
                'sort' => true,
            ],
            [
                'title' => 'Updated at',
                'attribute' => 'updated_at',
                'sort' => true,
            ],
        ];

        $columns[] = [
            'title' => 'tools',
            'sort' => false,
            'tools' => ['edit', 'delete'],
        ];

        return view('livewire.backstage.table', [
            'columns' => $columns,
            'resource' => 'winner-lines',
            'rows' => WinnerLine::search($this->search)
                ->orderBy($this->sortField, $this->sortAsc ? 'DESC' : 'ASC')
                ->paginate($this->perPage),
        ]);
    }
}
