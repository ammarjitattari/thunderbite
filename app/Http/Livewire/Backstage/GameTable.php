<?php

namespace App\Http\Livewire\Backstage;

use App\Models\Game;

class GameTable extends TableComponent
{
    public $sortField = 'revealed_at';
    /**
     * For columns
     */
    public $columns;

    /**
     * For Export button
     */
    public $canExport = true;

    public function render()
    {
        $this->columns = [
            [
                'title' => 'account',
                'sort' => true,
            ],
            [
                'title' => 'symbol_url',
                'field_type'=>'image',
                'url' => 'symbol_url',
                'sort' => false,
            ],
            [
                'attribute' => 'total_points',
                'title' => 'Total points',
                'sort' => true,
            ],
            [
                'title' => 'Winner line',
                'attribute' => 'winner_line',
                'sort' => false,
            ],
            [
                'title' => 'revealed at',
                'attribute' => 'revealed_at',
                'sort' => true,
            ],
        ];

        return view('livewire.backstage.table', [
            'columns' => $this->columns,
            'resource' => 'games',
            'rows' => Game::filter()
                ->orderBy($this->sortField, $this->sortAsc ? 'DESC' : 'ASC')
                ->paginate($this->perPage),
        ]);
    }
    
    /**
     * Export function to export the records
     */
    public function export()
    {
        $columns = [];

        //Getting record from database
        $games = Game::filter()->orderBy($this->sortField, $this->sortAsc ? 'DESC' : 'ASC')->get();

        //Setting header for CSV download
        $fileName = 'Games.csv';
            $headers = array(
            "Content-type"        => "text/csv",
            "Content-Disposition" => "attachment; filename=$fileName",
            "Pragma"              => "no-cache",
            "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
            "Expires"             => "0"
        );
        
        //Getting CSV headers
        foreach($this->columns as $column){
            $columns[] =strtoupper($column['title']);
        }
        
        // callback function for creating CSV rows
        $callback = function() use($games, $columns) {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);

            foreach ($games as $game) {
                foreach($this->columns as $key=>$column){
                    $fieldName = isset($column['attribute']) ? $column['attribute'] : $column['title'];
                    $row[$key] = $game->{$fieldName};
                }
                fputcsv($file, $row);
            }

            fclose($file);
        };

        return response()->stream($callback, 200, $headers);
    }
}
