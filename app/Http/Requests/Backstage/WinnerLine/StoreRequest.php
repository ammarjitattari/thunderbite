<?php

namespace App\Http\Requests\Backstage\WinnerLine;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use App\Rules\FormattedWinnerLine;
use App\Rules\UniqueWinnerLine;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'line' => ['required', new FormattedWinnerLine, new UniqueWinnerLine],
            'status' => ['required'],
        ];
    }
}
