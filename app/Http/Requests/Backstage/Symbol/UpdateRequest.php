<?php

namespace App\Http\Requests\Backstage\Symbol;

    use Illuminate\Foundation\Http\FormRequest;
    use Illuminate\Validation\Rule;

    class UpdateRequest extends FormRequest
    {
        /**
         * Determine if the user is authorized to make this request.
         *
         * @return bool
         */
        public function authorize()
        {
            return true;
        }

        /**
         * Get the validation rules that apply to the request.
         *
         * @return array
         */
        public function rules()
        {
            return [
                'symbol' => 'mimes:jpg,png,jpeg,gif,svg|max:2048',
                'status' => ['required'],
                'points' => 'required',
            ];
        }
    }
