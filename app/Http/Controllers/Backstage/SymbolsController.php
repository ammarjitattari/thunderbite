<?php

namespace App\Http\Controllers\Backstage;

use App\Http\Controllers\Controller;
use App\Models\Symbol;
use App\Http\Requests\Backstage\Symbol\{
    StoreRequest,
    UpdateRequest
};
use File;


class SymbolsController extends Controller
{

    /**
     * Symbol mode instance;
     */
    private $__symbol;
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('backstage.symbols.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('backstage.symbols.create', [
            'symbol' => new Symbol(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param App\Http\Requests\Backstage\Symbol\StoreRequest; $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        // Validation
        $symbol = new Symbol();
        $data = $request->only(['status', 'points']);
        
        if(!$symbol->canChangeStatus($request->status,$symbol->id)){
            if($request->status == Symbol::STATUS_ACTIVE){
                session()->flash('error', trans('validation.custom.symbol.max_actve_limit',['limit'=>Symbol::MAXIMUM_ACTIVE_SYMBOLS]));
            }else{
                session()->flash('error', trans('validation.custom.symbol.min_inactve_limit', ['limit'=>Symbol::MIMIMUM_ACTIVE_SYMBOLS]));
            }
            return redirect()->route('backstage.symbols.create')->withInput();
        }
        if ($request->hasFile('symbol')) {
            $filenameWithExt = $request->file('symbol')->getClientOriginalName ();
            // Get Filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            // Get just Extension
            $extension = $request->file('symbol')->getClientOriginalExtension();
            // Filename To store
            $fileNameToStore = $filename. '_'. time().'.'.$extension;
            $request->file('symbol')->move(public_path(Symbol::SYMBOL_FOLDER), $fileNameToStore);
            $data['symbol'] = $fileNameToStore;
        }
        // Create the Symbol
        Symbol::create($data);

        // Set message
        session()->flash('success', trans('messages.symbol.success.created'));
        // Redirect
        return redirect()->route('backstage.symbols.index');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Symbol $symbol
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Symbol $symbol)
    {
        return view('backstage.symbols.edit', compact('symbol'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateRequest $request
     * @param App\Http\Requests\Backstage\Symbol\UpdateRequest; $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateRequest $request, $id)
    {
        $symbol = Symbol::find($id);
        $data = $request->only(['status', 'points']);
        if(!$symbol->canChangeStatus($request->status,$id)){
            if($request->status == Symbol::STATUS_ACTIVE){
                session()->flash('error', trans('validation.custom.symbol.max_actve_limit',['limit'=>Symbol::MAXIMUM_ACTIVE_SYMBOLS]));
            }else{
                session()->flash('error', trans('validation.custom.symbol.min_inactve_limit', ['limit'=>Symbol::MIMIMUM_ACTIVE_SYMBOLS]));
            }
            return redirect()->route('backstage.symbols.create')->withInput();
        }
        
        if ($request->hasFile('symbol')) {
            $filenameWithExt = $request->file('symbol')->getClientOriginalName ();
            // Get Filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            // Get just Extension
            $extension = $request->file('symbol')->getClientOriginalExtension();
            // Filename To store
            $fileNameToStore = $filename. '_'. time().'.'.$extension;
            $p = $request->file('symbol')->move(public_path(Symbol::SYMBOL_FOLDER), $fileNameToStore);
            $data['symbol'] = $fileNameToStore;
        }
        // Update the symbol data
        $symbol->update($data);
        // Redirect
        session()->flash('success', trans('messages.symbol.success.updated'));

        return redirect()->route('backstage.symbols.edit', $symbol->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Symbol $symbol
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Symbol $symbol)
    {
        
        File::delete($symbol->symbol_path);
        $symbol->forceDelete();

        if (request()->ajax()) {
            return response()->json(['status' => 'success']);
        }
        session()->flash('success', trans('messages.symbol.success.deleted'));

        return redirect(route('backstage.symbols.index'));
    }

}
