<?php

namespace App\Http\Controllers\Backstage;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backstage\WinnerLine\{
    StoreRequest,
    UpdateRequest
};
use App\Models\WinnerLine;
use App\Rules\FormattedWinnerLine;
use App\Rules\UniqueWinnerLine;
use GuzzleHttp\Psr7\Request;


class WinnerLineController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('backstage.winnerline.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('backstage.winnerline.create', [
            'winnerLine' => new WinnerLine(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param App\Http\Requests\Backstage\WinnerLine\StoreRequest
     *
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        $data = $request->only(['line', 'status']);
        // Create the Symbol
        WinnerLine::create($data);

        // Set message
        session()->flash('success', trans('messages.winner_line.success.created'));

        // Redirect
        return redirect()->route('backstage.winner-lines.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param WinnerLine $winnerLine
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(WinnerLine $winnerLine)
    {
        return view('backstage.winnerline.edit', compact('winnerLine'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param App\Http\Requests\Backstage\WinnerLine\StoreRequest
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(StoreRequest $request, $id)
    {
        $winnerLine = WinnerLine::find($id);
        //Getting values from request
        $data = $request->only(['line', 'status']);
        // Update the symbol data
        $winnerLine->update($data);
        // Redirect
        session()->flash('success', trans('messages.winner_line.success.updated'));

        return redirect()->route('backstage.winner-lines.edit', $winnerLine->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param WinnerLine $winnerLine
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(WinnerLine $winnerLine)
    {
        $winnerLine->forceDelete();

        if (request()->ajax()) {
            return response()->json(['status' => 'success']);
        }
        session()->flash('success', trans('messages.winner_line.success.deleted'));

        return redirect(route('backstage.winner-lines.index'));
    }

}
