<?php

namespace App\Http\Controllers;

use App\Models\Campaign;
use App\Models\Game;

class FrontendController extends Controller
{
    public function loadCampaign(Campaign $campaign)
    {
        return view('frontend.index')
        ->with('campaign',$campaign)
        ->with('campaignUser',request()->a)
        ->with('spin',request()->spin);
    }

    public function placeholder()
    {
        return view('frontend.placeholder');
    }

    

}