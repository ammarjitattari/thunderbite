<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ env('APP_NAME') }}</title>

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link href="{{ mix('/css/backstage.css') }}" rel="stylesheet">

    @livewireStyles
    <style>
        .button-create {
        background: rgb(255, 125, 59);
        background: linear-gradient(0deg, rgba(255, 125, 59, 1) 0%, rgba(255, 147, 53, 1) 90%);
        margin-left: 0.5rem;
        display: inline-block;
        border-radius: 0.5rem;
        padding-left: 1.5rem;
        padding-right: 1.5rem;
        padding-top: 0.75rem;
        padding-bottom: 0.75rem;
        line-height: 1;
        --tw-text-opacity: 1;
        color: rgb(255 255 255 / var(--tw-text-opacity));
    }
    .spinner-wrapper{
        text-align: center;
        margin: 10%;
        }
    </style>
</head>

<body class="bg-primary">
@livewire('spinner', ['campaignUser'=>$campaignUser, 'campaign'=>$campaign, 'spin' =>$spin])
@livewireScripts


</body>

</html>
