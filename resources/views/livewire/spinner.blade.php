<div class="spinner-wrapper">
   @if($canShowSpinner)
      <button wire:click="saveSpinner" class="button-create" wire:loading.attr="disabled">Spinner <img wire:loading wire:target="saveSpinner" src="{{asset('img/loader.gif')}}" width="20" height="20"></button>
   @endif
   
        
   
   @if($message)
      <h2>{{$message}}</h2>
   @endif
   
</div>
