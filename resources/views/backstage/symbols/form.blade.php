@csrf

@include('backstage.partials.forms.image', [
    'field' => 'symbol',
    'label' => 'Symbol',
    'cta' => $symbol->symbol ? 'Click to change':'Click to upload',
    'full_path' => $symbol->symbol ? $symbol->symbol_url:''
])

@include('backstage.partials.forms.text', [
    'field' => 'points',
    'label' => 'Points',
    'value' => old('points') ?? $symbol->points,
])

@include('backstage.partials.forms.select', [
    'field' => 'status',
    'label' => 'Status',
    'value' => old('timezone') ?? $symbol->status,
    'options'=> $symbol->getStatus()
])

@include('backstage.partials.forms.submit')
