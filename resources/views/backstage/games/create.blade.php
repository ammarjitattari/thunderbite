@extends('backstage.templates.backstage')

@section('tools')
    <a href="{{ route('backstage.games.index') }}" class="button-default">Games</a>
@endsection

@section('content')
    <div id="card" class="bg-white shadow-lg mx-auto rounded-b-lg">
        <div class="px-10 pt-4 pb-8">
            <h1>Create a new game</h1>
            <form method="POST" action="{{ route('backstage.games.store') }}">
                @include('backstage.games.form')
            </form>
        </div>
    </div>
@endsection
