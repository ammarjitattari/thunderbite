@csrf

@include('backstage.partials.forms.text', [
    'field' => 'line',
    'label' => 'Winner line',
    'helptext'=> 'Enter value separated by hyphen(-) like 1-2-3-4-5.',
    'value' => old('line') ?? $winnerLine->line_string,
])

@include('backstage.partials.forms.select', [
    'field' => 'status',
    'label' => 'Status',
    'value' => old('timezone') ?? $winnerLine->status,
    'options'=> $winnerLine->getStatus()
])

@include('backstage.partials.forms.submit')
