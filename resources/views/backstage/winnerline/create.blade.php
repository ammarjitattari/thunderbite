@extends('backstage.templates.backstage')

@section('tools')
    <a href="{{ route('backstage.winner-lines.index') }}" class="button-default">Winner lines</a>
@endsection

@section('content')
    <div id="card" class="bg-white shadow-lg mx-auto rounded-b-lg">
        <div class="px-10 pt-4 pb-8">
            <h1>Create a new winner line</h1>
            <form method="POST" action="{{ route('backstage.winner-lines.store') }}">
                @include('backstage.winnerline.form')
            </form>
        </div>
    </div>
@endsection
