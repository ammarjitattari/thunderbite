<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Message Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'spinner.message.winner' => 'Congratulations!, You have won :total_points points.',
    'spinner.message.lost' => 'You have lost. Best luck for next time.',
    'next' => 'Next &raquo;',
    'winner_line' =>[
        'success' =>[
            'created' =>  'The winner line has been created!',
            'updated' =>  'The winner line details have been saved!',
            'deleted' =>  'The winner line has been removed!'
        ]
    ],
    'symbol' =>[
        'success' =>[
            'created' =>  'The symbol has been created!',
            'updated' =>  'The symbol details have been saved!',
            'deleted' =>  'The symbol has been removed!'
        ]
    ]

];
